<?php
// whmcs module created by altomarketing.com.ar
// latest version

function centoswebpanel_ConfigOptions() {
  $configarray = array(
    "PACKAGE-NUMBER" => array( "Type" => "text", "Description" => "Package ID", "Default" => "1"),
    "inode" => array( "Type" => "text" , "Description" => "Max of inode", "Default" => "0",),
    "nofile" => array( "Type" => "text", "Description" => "Max of nofile", "Default" => "100", ),
    "nproc" => array( "Type" => "text" , "Description" => "Nproc limit - 40 suggested", "Default" => "40",),
  );
  return $configarray;
}

function centoswebpanel_CreateAccount($params) {
    if ($params["server"] == 1) {
        $postvars = array(
            'package' => $params["configoption1"],
            'domain' => $params["domain"],
            'key' => $params["serveraccesshash"],
            'action' => 'add',
            'username' => $params["username"],
            'user' => $params["username"],
            'pass' => $params["password"],
            'email' => $params["clientsdetails"]["email"],
            'inode' => $params["configoption2"],
            'nofile' => $params["configoption3"],
            'nproc' => $params["configoption4"],
            'server_ips'=>$params["serverip"]
        );
        $postdata = http_build_query($postvars);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://' . $params["serverhostname"] . ':2304/v1/account');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
        $answer = curl_exec($curl);
        logModuleCall('cwpwhmcs','CreateAccount_UserAccount','https://' . $params["serverhostname"] . ':2304/v1/account/'.$postdata,$answer);
    }
    if(strpos($answer,"OK")!==false){$result='success';}else{$result=json_decode($answer,true); $result=$result['msj'];}
    return $result;
}
function centoswebpanel_TerminateAccount($params) {
    if ($params["server"] == 1) {
        $postvars = array('key' => $params["serveraccesshash"],'action' => 'del','user' => $params["username"]);
        $postdata = http_build_query($postvars);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://' . $params["serverhostname"] . ':2304/v1/account');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
        $answer = curl_exec($curl);
        logModuleCall('cwpwhmcs','TerminateAccount','https://' . $params["serverhostname"] . ':2304/v1/account/'.$postdata,$answer);
    }
    if(strpos($answer,"OK")!==false){$result='success';}else{$result=json_decode($answer,true); $result=$result['msj'];}
    return $result;
}
function centoswebpanel_SuspendAccount($params) {
    if ($params["server"] == 1) {
        $postvars = array('key' => $params["serveraccesshash"],'action' => 'susp','user' => $params["username"]);
        $postdata = http_build_query($postvars);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://' . $params["serverhostname"] . ':2304/v1/account');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
        $answer = curl_exec($curl);
    }
    if(strpos($answer,"OK")!==false){$result='success';}else{$result=json_decode($answer,true); $result=$result['msj'];}
    logModuleCall('cwpwhmcs','SuspendAccount','https://' . $params["serverhostname"] . '::2304/v1/account/'.$postdata,$result);
    return $result;
}
function centoswebpanel_UnsuspendAccount($params) {
    if ($params["server"] == 1) {
        $postvars = array('key' => $params["serveraccesshash"],'action' => 'unsp','user' => $params["username"]);
        $postdata = http_build_query($postvars);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://' . $params["serverhostname"] . ':2304/v1/account');
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
        $answer = curl_exec($curl);
    }
    if(strpos($answer,"OK")!==false){$result='success';}else{$result=json_decode($answer,true); $result=$result['msj'];}
    logModuleCall('cwpwhmcs','UnsuspendAccount','https://' . $params["serverhostname"] . ':2304/v1/account'.$postdata,$result);
    return $result;
}
function centoswebpanel_ClientArea($params) {

    $postvars = array('key' => $params["serveraccesshash"],'action' => 'list','user' => $params["username"],'timer'=>5);
    $postdata = http_build_query($postvars);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://' . $params["serverhostname"] . ':2304/v1/user_session');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
    $answer = curl_exec($curl);
    $arry=(json_decode($answer,true)); //die;
    $link=$arry['msj']['details'];
    $linkautologin="https://{$params["serverhostname"]}:2083/{$link[0]['user']}/?user_session={$link[0]['token']}";
    //logModuleCall('cwpwhmcs','centoswebpanel_LoginLink','https://' . $params["serverhostname"] . ':2304/v1/user_session'.$postdata,$answer);


    $code = '<form action="'.$linkautologin.'" method="GET" target="_blank">
        <input type="submit" value="Login to Control Panel" />
        <input type="hidden" id="user_session" name="user_session" value="'.$link[0]['token'].'" />
        <input type="button" value="Login to Webmail" onClick="window.open(\'https://'.$params["serverhostname"].'/webmail\')" />
        </form>';
    return $code;
}
function centoswebpanel_AdminLink($params) {
    $code = '<form action="https://'.$params["serverhostname"].':2031" method="post" target="_blank">
        <input type="submit" value="Login to Control Panel" />
        </form>';
    return $code;
}
function centoswebpanel_LoginLink($params) {
    $postvars = array('key' => $params["serveraccesshash"],'action' => 'list','user' => $params["username"],'timer'=>5);
    $postdata = http_build_query($postvars);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://' . $params["serverhostname"] . ':2304/v1/user_session');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
    $answer = curl_exec($curl);
    $arry=(json_decode($answer,true)); //die;
    $link=$arry['msj']['details'];
    $linkautologin="https://{$params["serverhostname"]}:2083/{$link[0]['user']}/?user_session={$link[0]['token']}";
    logModuleCall('cwpwhmcs','centoswebpanel_LoginLink','https://' . $params["serverhostname"] . ':2304/v1/user_session'.$postdata,$answer);

    echo "<a href=\"{$linkautologin}\" target=\"_blank\" style=\"color:#cc0000\">Control Panel</a>";
}
function centoswebpanel_ChangePassword($params){
    $postvars = array('key' => $params["serveraccesshash"],'acction' => 'udp','user' => $params["username"], 'pass' =>$params["password"]);
    $postdata = http_build_query($postvars);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, 'https://'. $params["serverhostname"] . ':2304/v1/changepass');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
    $answer = curl_exec($curl);
    if(strpos($answer,"OK")!==false){$result='success';}else{$result=json_decode($answer,true); $result=$result['msj'];}
    logModuleCall('cwpwhmcs','ChangePassword','https://' . $params["serverhostname"] . ':2304/v1/changepass'.$postdata,$result);
    return $result;
}
function centoswebpanel_ChangePackage($params){
    $postvars = array("key" => $params["serveraccesshash"],"action"=>'udp','user' => $params["username"],'package'=>$params["configoption1"].'@');
    $postdata = http_build_query($postvars);
    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, 'https://'. $params["serverhostname"] . ':2304/v1/account');
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
    $answer = curl_exec($curl);
    curl_close($curl);

    if(strpos($answer,"OK")!==false){$result='success';}else{$result=json_decode($answer,true); $result=$result['msj'];}
    logModuleCall('cwpwhmcs','ChangePackage','https://' . $params["serverhostname"] . ':2304/v1/packages'.$postdata,$answer);
    return $result;
}
?>