# Centos-webpanel server module for Whmcs with Bridge


#### Open Source with optional support

This is a opensource script, but if you need [you can request comercial support and custom installation here](https://www.paneldecliente.com/cart.php?a=add&pid=176) 

We guarantee that this module works excellent and will be installed in few minutes

- [For errors read here](errors)
- [or request custom support](https://www.paneldecliente.com/cart.php?a=add&pid=176)
- [Please report bugs or request new features here](https://gitlab.com/whmcs-proyects/servers/centos-webpanel/issues)

----

## How to install it

Download WHMCS module for CWP from this site

- Extract file centoswebpanel.zip in your whmcs folder WHMCS/modules/servers/ as extracted module file path will look like this:

WHMCS/modules/servers/centoswebpanel/centoswebpanel.php

How to configure it:

## First Inside your CWP Server

### Generate API Key on the CWP server

goto Left-Menu -> CWP Settings -> API Manager
Acept WHMCS permision or manually Use the following permissions:
```
-add, edit, suspend, unsuspend and delete accounts
-edit packages
-change Password
```


### Firewall in your Cwp Server
1. Add your whmcs ip to your csf or firewall inside your cwp
2. _For the correct operation of this tool you must open port 2304 in the firewall_  After that , restart your csf

# Now go to your WHMCS Server

 In Menu goto: Setup -> Products/Services -> Servers
Add New Server and under “Server Details”
```
Type = Cwp7
Username: root
Password: (your server password)
Access Hash: copy your key from CWP API generated as mentioned
```
. 
### Firewall in your Whcms Server
1. Add your whmcs ip to your csf or firewall inside your cwp
2. _For the correct operation of this tool you must open port 2304 in the firewall_  After that , restart your csf
